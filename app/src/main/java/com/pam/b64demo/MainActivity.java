package com.pam.b64demo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Get b64 string
        String b64ImageString = getImageString(this);

        //Decode string to bitmap image
        Bitmap imageBitmap = b64StringToBitmap(b64ImageString);

        //Set bitmap to imageview
        ImageView imageView = (ImageView) findViewById(R.id.imageViewOriginal);
        imageView.setImageBitmap(imageBitmap);

        ((TextView)findViewById(R.id.textView)).setText("Original: " + imageBitmap.getWidth() + "x" + imageBitmap.getHeight() +
                                                        ", size: "+ b64ImageString.length());

        //-------------

        //Resize bitmap to lower resolution and ultimately reduce b64 string size
        Bitmap resizedBitmap = resizeBitmap(imageBitmap, 400);

        //Encode Bitmap to b64 String
        String b64EncodedImage = bitmapToB64String(resizedBitmap);

        //Decode b64 string to Bitmap to show it works
        Bitmap decodedResizedImage = b64StringToBitmap(b64EncodedImage);

        //Assign decoded string to imageview
        ImageView imageViewResized = (ImageView) findViewById(R.id.imageViewResized);
        imageViewResized.setImageBitmap(decodedResizedImage);

        ((TextView)findViewById(R.id.textView2)).setText("Resized: " + decodedResizedImage.getWidth() + "x" + decodedResizedImage.getHeight() +
                                                         ", size: " + b64EncodedImage.length());
    }


    private Bitmap resizeBitmap(Bitmap bitmap, int maxSize) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int newHeight;
        int newWidth;
        if (width < height) {
            newHeight = maxSize;
            newWidth = (width * newHeight) / height;
        } else {
            newWidth = maxSize;
            newHeight = (height * newWidth) / width;
        }

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);
    }

    private Bitmap b64StringToBitmap(String b64String) {
        byte[] imageBytes = Base64.decode(b64String, Base64.DEFAULT);

        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

    private String bitmapToB64String(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayStream);
        byte[] imageBytes = byteArrayStream.toByteArray();

        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    public String getImageString(Context context) {

        try {
            StringBuilder buf = new StringBuilder();
            InputStream txt = context.getResources().openRawResource(R.raw.b64_image);
            BufferedReader in = new BufferedReader(new InputStreamReader(txt, "UTF-8"));
            String str;

            while ((str=in.readLine()) != null) {
                buf.append(str);
            }

            in.close();

            return buf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
